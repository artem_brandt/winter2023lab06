public class Board {
	
	//fields
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	//constructor
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	//overridden toString
	public String toString() {
		String output = "";
		for (int i = 1; i <=12; i++) {
			if (tiles[i - 1] == false) {
				output += i + " ";
			}
			else {
				output += "X ";
			}
		}
		return output;
	}
	
	public boolean playATurn() {
		
		//Rolling dice (5.a,b)
		die1.roll();
		die2.roll();
		System.out.println("First Die: " + die1 + ", Second Die: " + die2);
		
		//Checking SUM (5.d,e)
		int diceSUM = die1.getFaceValue() + die2.getFaceValue();
		if (diceSUM <= tiles.length && !(tiles[diceSUM - 1])) {
			tiles[diceSUM - 1] = true;
			System.out.println("Closing tile equal to sum: " + diceSUM);
			return false;
		}
		
		//Checking Die1 (5.f)
		if (!tiles[die1.getFaceValue()-1]) {
        tiles[die1.getFaceValue()-1] = true;
        System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
        return false;
		}
		
		//Checking Die2 (5.g)
		if (!tiles[die2.getFaceValue()+5]) {
        tiles[die2.getFaceValue()+5] = true;
        System.out.println("Closing tile with the same value as die two: " + (die2.getFaceValue()+6));
        return false;
		}
		
		//If everything is shut (5.h)
		System.out.println("All the tiles for these values are already shut");
		return true;
	}
	
}