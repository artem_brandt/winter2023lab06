public class Jackpot {
	public static void main (String[] args) {
		
		System.out.println("Welcome to Jackpot game!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while (gameOver == false) {
			System.out.println(board);
			if (board.playATurn()) {
				gameOver = true;
			}
			else {
				numOfTilesClosed++;
			}
		}
		
		if (numOfTilesClosed >= 7) {
			System.out.println("You reached the Jackpot and won!");
		}
		else {
			System.out.println("You didn't reach the Jackpot and lost.");
		}
	}
}