import java.util.Random;

public class Die {
	
	//fields
	private int faceValue;
	private Random rand;
	
	//constructor
	public Die() {
		faceValue = 1;
		this.rand = new Random();
	}
	
	//get methods
	public int getFaceValue() {
		return this.faceValue;
	}
	
	//faceValue setter
	public void roll() {
		this.faceValue = rand.nextInt(6) + 1;
	}
	
	//toString method
	public String toString() {
		return String.valueOf(this.faceValue);
	}
}